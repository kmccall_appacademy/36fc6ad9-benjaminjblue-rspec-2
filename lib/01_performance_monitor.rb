require "time"

def measure(iterations = 1, &block)
    before = Time.now
    iterations.times { block.call }
    (Time.now - before) / iterations
end
