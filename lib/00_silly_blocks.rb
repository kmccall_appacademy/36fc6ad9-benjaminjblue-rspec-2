def reverser(&str)
    str.call.split(' ').map{|word|
        (word.length - 1).downto(0).reduce(''){|reverse, i|
            reverse + word[i]
        }
    }.join(' ')
end

def adder(num = 1, &num2)
    num + num2.call
end

def repeater(times = 2, &block)
    (0...times).each do
        block.call
    end
end

